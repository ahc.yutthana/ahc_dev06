﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dto.User
{
    public class NoticeDto
    {
        public int id { get; set; }
        public string name { get; set; }
        public string comment { get; set; }
        public string status { get; set; }
        public string create_date { get; set; }
    }
}

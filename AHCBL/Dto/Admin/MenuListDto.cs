﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dto.Admin
{
    public class MenuListDto
    {
        public int id { get; set; }
        public string name { get; set; }
        public string icon { get; set; }
        public List<MenuList> sub_menu { get; set; }

        public MenuListDto()
        {
            sub_menu = new List<MenuList>();
        }

    }
    public class MenuList
    {
        public int id { get; set; }
        public string name { get; set; }
        public string url { get; set; }
       
    }

}

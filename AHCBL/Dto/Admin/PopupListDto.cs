﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AHCBL.Dto.Admin
{
    public class PopupListDto
    {
        public int id { get; set; }

        [MaxLength(255)]
        [Required(ErrorMessage = "Please enter.")]
        public string name { get; set; }
        public int grp_id { get; set; }
        public int drive_id { get; set; }
        public string drive_name{ get; set; }

        [MaxLength(5)]
        public string disable_hours { get; set; }
        [Required(ErrorMessage = "Please enter.")]
        public string start_date { get; set; }
        [Required(ErrorMessage = "Please enter.")]
        public string stop_date { get; set; }

        [MaxLength(5)]
        public string pop_left { get; set; }

        [MaxLength(5)]
        public string pop_top { get; set; }

        [MaxLength(5)]
        public string pop_width { get; set; }

        [MaxLength(5)]
        public string pop_height { get; set; }

        [AllowHtml]
        [MaxLength(500)]
        public string detail { get; set; }

    }
}

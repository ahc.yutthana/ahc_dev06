﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AHCBL.Dto.Admin
{
    public class ContentListDto
    {
        public int id { get; set; }

        [MaxLength(20)]
        public string code { get; set; }

        [MaxLength(70)]
        public string name { get; set; }

        [AllowHtml]
        [MaxLength(255)]
        public string contents { get; set; }

        [AllowHtml]
        [MaxLength(255)]
        public string contents_mobile { get; set; }
        public int skin_directory { get; set; }
        public SelectList skin_directory_list { get; set; }        
        public int skin_directory_mobile { get; set; }
        public SelectList skin_directory_mobile_list { get; set; }

        [MaxLength(60)]
        public string include_head { get; set; }

        [MaxLength(60)]
        public string include_tail { get; set; }

        [MaxLength(255)]
        public string img_head { get; set; }

        [MaxLength(255)]
        public string img_tail { get; set; }
        public int active { get; set; }
        public string create_date { get; set; }
        public bool use { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AHCBL.Dto.Admin
{
    public class QAConfigDto
    {
        public int id { get; set; }

        [MaxLength(255)]
        public string title { get; set; }

        [MaxLength(255)]
        public string category { get; set; }
        public int directory_skin { get; set; }
        public int mobile_directory_skin { get; set; }
        public bool use_email { get; set; }
        public bool req_email{ get; set; }
        public bool use_sms { get; set; }
        public bool use_hp { get; set; }
        public bool req_hp { get; set; }

        [MaxLength(30)]
        public string send_number { get; set; }

        [MaxLength(30)]
        public string mobile_admin { get; set; }

        [MaxLength(50)]
        public string email_admin { get; set; }

        [MaxLength(1)]
        public string use_editor { get; set; }
        public int subject_len { get; set; }
        public int mobile_subject_len { get; set; }
        public int page_rows { get; set; }
        public int mobile_page_rows { get; set; }
        public int image_width { get; set; }
        public int upload_size { get; set; }

        [MaxLength(50)]
        public string include_head { get; set; }

        [MaxLength(50)]
        public string include_tail { get; set; }

        [AllowHtml]
        [MaxLength(500)]
        public string detail_hots { get; set; }

        [AllowHtml]
        [MaxLength(500)]
        public string detail_bottom { get; set; }

        [AllowHtml]
        [MaxLength(500)]
        public string mobile_detail_hots { get; set; }

        [AllowHtml]
        [MaxLength(500)]
        public string mobile_detail_bottom { get; set; }

        [MaxLength(500)]
        public string insert_content { get; set; }

        [MaxLength(250)]
        public string subj1 { get; set; }

        [MaxLength(250)]
        public string subj2 { get; set; }

        [MaxLength(250)]
        public string subj3 { get; set; }

        [MaxLength(250)]
        public string subj4 { get; set; }

        [MaxLength(250)]
        public string subj5 { get; set; }

        [MaxLength(250)]
        public string txt1 { get; set; }

        [MaxLength(250)]
        public string txt2 { get; set; }

        [MaxLength(250)]
        public string txt3 { get; set; }

        [MaxLength(250)]
        public string txt4 { get; set; }

        [MaxLength(250)]
        public string txt5 { get; set; }

        public SelectList skin_list { get; set; }
        public SelectList mobile_skin_list { get; set; }

        public SelectList use_sms_list { get; set; }
        public SelectList use_editor_list { get; set; }

    }
}

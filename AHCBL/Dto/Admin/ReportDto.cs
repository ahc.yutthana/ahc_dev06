﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dto.Admin
{
    public class ReportDto
    {
        public int id { get; set; }
        public string send_name { get; set; }
        public string recip_name { get; set; }
        public string name { get; set; }
        public string process { get; set; }
        public string username { get; set; }
        public string account { get; set; }
        public string create_date { get; set; }
        public string code { get; set; }

        public string cer { get; set; }
        public string chk_win { get; set; }
        public string user_win { get; set; }
        public string user_sell { get; set; }
        public string detail { get; set; }
    }
}

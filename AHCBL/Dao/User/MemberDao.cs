﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.User
{
    public class MemberDao : BaseDao<MemberDao>
    {
        private MySqlConnection conn;
        public string ChangeBank(MemberListDto model)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD051_CHANGE_BANK_MEMBER", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
                AddSQLParam(param, "@bank_id", Util.NVLInt(model.bank_id));
                AddSQLParam(param, "@acc_no", Util.NVLString(model.acc_no));
                AddSQLParam(param, "@acc_name", Util.NVLString(model.acc_name));
                AddSQLParam(param, "@member_id", Util.NVLInt(Varible.User.member_id));

                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }

        public string  ChangePass(int id, string pass)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD052_CHANGE_PASS_MEMBER", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(id));
                AddSQLParam(param, "@pass", Util.NVLString(DataCryptography.Encrypt(pass.ToString().Trim())));
                AddSQLParam(param, "@member_id", Util.NVLInt(Varible.User.member_id));

                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
    }
}

﻿using AHCBL.Component.Common;
using AHCBL.Dto;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao
{
    public class ChartMemberDao : BaseDao<ChartMemberDao>
    {
        private DataTable dt;
       
        public List<ChartMemberDto> GetChartData()
        {
            try
            {
                List<ChartMemberDto> list = new List<ChartMemberDto>();
                dt = GetStoredProc("PD073_GET_CHART_MEMBER");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(new ChartMemberDto
                    {
                        id = Util.NVLInt(dr["member_id"]),
                        username = Util.NVLString(dr["username"]),
                        adviser_id = Util.NVLInt(dr["adviser_id"]),
                        email = Util.NVLString(dr["email"]),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        master_id = Util.NVLInt(dr["master_id"])
                        //Util.NVLInt(dr["member_id"]), Util.NVLString(dr["username"]), Util.NVLString(dr["adviser_id"]), Util.NVLString(dr["email"]), Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss")))
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }

    }
}
﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.Admin
{
    public class DashboardDao : BaseDao<DashboardDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public DashboardDto GetData()
        {
            try
            {
                DashboardDto data = new DashboardDto();
                dt = GetStoredProc("PD042_GET_MEMBER_NEW");
                foreach (DataRow dr in dt.Rows)
                {
                    data.tab1.Add(new tb1 {
                        username = Util.NVLString(dr["username"]),
                        fullname = Util.NVLString(dr["fullname"]),
                        nickname = Util.NVLString(dr["nickname"]),
                        member_no = Util.NVLString(dr["member_no"]),
                        point = Util.NVLInt(dr["point"])
                    });                   
                }
                dt = GetStoredProc("PD040_GET_POST_NEW");
                foreach (DataRow dr in dt.Rows)
                {
                    data.tab2.Add(new tb2
                    {
                        grp_name = Util.NVLString(dr["grp_name"]),
                        app_name = Util.NVLString(dr["app_name"]),
                        title = Util.NVLString(dr["title"]),
                        create_by = Util.NVLString(dr["create_by"]),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss")))
                    });
                }
                dt = GetStoredProc("PD041_GET_POINT_NEW");
                foreach (DataRow dr in dt.Rows)
                {
                    data.tab3.Add(new tb3
                    {
                        p_username = Util.NVLString(dr["p_username"]),
                        p_name = Util.NVLString(dr["p_name"]),
                        p_nickname = Util.NVLString(dr["p_nickname"]),
                        p_create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["p_create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        p_detail = Util.NVLString(dr["p_detail"]),
                        p_amount = Util.NVLString(dr["p_amount"]),

                        p_total = Util.NVLString(dr["p_total"])
                    });
                }
                return data;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
    }
}
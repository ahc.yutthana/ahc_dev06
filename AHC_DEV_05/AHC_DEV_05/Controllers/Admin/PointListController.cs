﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto.Admin;
using OfficeOpenXml;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class PointListController : Controller
    {
        Permission checkuser = new Permission();
        // GET: PointList
        public ActionResult Index(string drp, string keyword, int? page)
        {
            checkuser.chkrights("admin");
            var data = PointListDao.Instance.GetDataList(0);
            var amount = GetDataDao.Instance.GetPointAmount();
            Session["dropdown"] = DropdownDao.Instance.GetDrpPoint(0);
            int rows = Util.NVLInt(Varible.Config.page_rows);
                        
            if (amount < 1000)
            {
                Session["total"] = amount.ToString();
            }
            else
            {
                Session["total"] = amount.ToString("0,0", CultureInfo.InvariantCulture);
            }
            if (data.Count < 1000)
            {
                Session["count"] = data.Count.ToString();
            }
            else
            {
                Session["count"] = data.Count.ToString("0,0", CultureInfo.InvariantCulture);
            }

            ViewBag.Amount = Session["total"];
            ViewBag.Count = Session["count"];
            ViewBag.Search = Session["dropdown"];
            ViewBag.Rows = rows;
            TempData["data"] = data.ToList().ToPagedList(page ?? 1, rows);
            TempData["data1"] = data.ToList();

            if (drp == "username")
            {
                TempData["data"] = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
                TempData["data1"] = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList();

                //return View(data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, rows));
            }
            if (drp == "name")
            {
                TempData["data"] = data.Where(x => x.name == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
                TempData["data1"] = data.Where(x => x.name == keyword || keyword == null || keyword == "").ToList();
                //return View(data.Where(x => x.name == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, rows));
            }
            Session["data"] = TempData["data1"];
            return View(TempData["data"]);
            //return View(data.ToList().ToPagedList(page ?? 1, rows));

        }
        public ActionResult Delete(PointListDto model, string[] data)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    foreach (string id in data)
                    {
                        model.id = Util.NVLInt(id);
                        PointListDao.Instance.SaveDataList(model, "del");
                    }
                    return Json("Deleted successfully !");
                }
                else
                {
                    return Json("Data not found !");
                }
            }
            catch (Exception e)
            {
                return Json("Error");
            }
        }
        public ActionResult ExportToExcel()
        {
            try
            {
                var data = Session["data"] as List<PointListDto>;
                if (data != null)
                {
                    ExcelPackage.LicenseContext = LicenseContext.Commercial;
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    ExcelPackage Ep = new ExcelPackage();
                    ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
                    Sheet.Cells["A1"].Value = "회원아이디";
                    Sheet.Cells["B1"].Value = "이름";
                    Sheet.Cells["C1"].Value = "복숭아 내용";
                    Sheet.Cells["D1"].Value = "복숭아";
                    Sheet.Cells["E1"].Value = "일시";
                    Sheet.Cells["F1"].Value = "만료일";
                    Sheet.Cells["G1"].Value = "복숭아합";
                    int row = 2;
                    foreach (var item in data)
                    {
                        Sheet.Cells[string.Format("A{0}", row)].Value = item.username.ToString();
                        Sheet.Cells[string.Format("B{0}", row)].Value = item.fullname.ToString();
                        Sheet.Cells[string.Format("C{0}", row)].Value = item.name.ToString();
                        Sheet.Cells[string.Format("D{0}", row)].Value = item.amount.ToString();
                        Sheet.Cells[string.Format("E{0}", row)].Value = item.create_date.ToString();
                        Sheet.Cells[string.Format("F{0}", row)].Value = item.exp_date.ToString();
                        Sheet.Cells[string.Format("G{0}", row)].Value = item.total.ToString();
                        row++;
                    }
                    Sheet.Cells["A:AZ"].AutoFitColumns();
                    Response.Clear();
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment: filename=" + "Report.xlsx");
                    Response.BinaryWrite(Ep.GetAsByteArray());
                    Response.End();


                }


                ViewBag.Amount = Session["total"];
                ViewBag.Count = Session["count"];
                ViewBag.Search = Session["dropdown"];
                ViewBag.Rows = Util.NVLInt(Varible.Config.page_rows);

                return View("Index");
            }
            catch (Exception e)
            {
                return RedirectToAction("Index");
            }
        }
    }
}
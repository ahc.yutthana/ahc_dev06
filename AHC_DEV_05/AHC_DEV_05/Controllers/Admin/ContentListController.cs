﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto.Admin;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class ContentListController : Controller
    {
        Permission checkuser = new Permission();
        // GET: ContentList
        public ActionResult Index(int? page)
        {
            checkuser.chkrights("admin");
            var data = ContentListDao.Instance.GetDataList();
            string count = string.Empty;
            if (data.Count < 1000)
            {
                count = data.Count.ToString();
            }
            else
            {
                count = data.Count.ToString("0,0", CultureInfo.InvariantCulture);                
            }

            ViewBag.Count = count;
            ViewBag.IdContentUse = data.Where(i => i.use == true).Select(i => i.id).FirstOrDefault();
            ViewBag.SwitchContent = data.Where(i => i.use == true).Select(i => i.id).Count() == 1? "On":"Off"; 
            return View(data.ToList().ToPagedList(page ?? 1, Util.NVLInt(ConfigurationManager.AppSettings["rows"])));
        }

        // GET: ContentList/Details/5
        public ActionResult Details(int id)
        {
            var model = ContentListDao.Instance.GetDataList().Find(smodel => smodel.id == id);
            return View(model);
        }

        // GET: ContentList/Create
        public ActionResult Create()
        {
            var Skin = DropdownDao.Instance.GetSkin();
            ViewBag.skin_directory = Skin;
            ViewBag.skin_directory_mobile = Skin;
            return View();
        }

        // POST: ContentList/Create
        [HttpPost]
        public ActionResult Create(ContentListDto model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    HttpPostedFileBase img_head = Request.Files["img_head"];
                    HttpPostedFileBase img_tail = Request.Files["img_tail"];

                    var head = FileUploadDao.Instance.Upload(img_head);
                    var tail = FileUploadDao.Instance.Upload(img_tail);
                    model.img_head = head.name;
                    model.img_tail = tail.name;

                    string result = ContentListDao.Instance.SaveDataList(model, "add");
                    if (result != "OK")
                    {
                        ViewBag.Message = result;
                        ModelState.Clear();
                    }
                    else
                    {
                        //ViewBag.Status = TempData["Dropdown"];
                        var Skin = DropdownDao.Instance.GetSkin();
                        ViewBag.skin_directory = Skin;
                        ViewBag.skin_directory_mobile = Skin;

                        ViewBag.Message = "Successfully !!";
                        //KRPBL.Component.Common.Form.SetAlertMessage(this,"ไม่พบ User ID ที่ระบุ กรุณาตรวจสอบ");
                        ModelState.Clear();
                    }
                }
                return View();
            }
            catch (Exception e)
            {
                ViewBag.Message = "Error : " + e.Message;
                return View();
            }
        }

        // GET: ContentList/Edit/5
        public ActionResult Edit(int id)
        {
            var Skin = DropdownDao.Instance.GetSkin();
            var model = ContentListDao.Instance.GetDataList().Find(smodel => smodel.id == id);
           
            model.skin_directory_list = new SelectList(Skin, "Value", "Text");
            model.skin_directory_mobile_list = new SelectList(Skin, "Value", "Text");
            return View(model);
        }

        // POST: ContentList/Edit/5
        [HttpPost]
        public ActionResult Edit(ContentListDto model)
        {
            try
            {
                HttpPostedFileBase img_head = Request.Files["img_head"];
                HttpPostedFileBase img_tail = Request.Files["img_tail"];

                var head = FileUploadDao.Instance.Upload(img_head);
                var tail = FileUploadDao.Instance.Upload(img_tail);
                model.img_head = head.name;
                model.img_tail = tail.name;

                string result = ContentListDao.Instance.SaveDataList(model, "edit");
                if (result != "OK")
                {
                    ViewBag.Message = result;
                }
                else
                {
                    return RedirectToAction("Index");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(ContentListDto model)
        {
            try
            {
                string result = ContentListDao.Instance.SaveDataList(model, "del");
                if (result == "OK")
                {
                    ViewBag.Message = "Student Deleted Successfully";
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                ViewBag.Message = "Error : " + e.Message.ToString();
                return View();
            }
        }

        public ActionResult UpdateContentOff(bool val)
        {
            try
            {
                if (!val)
                {
                    string result = ContentListDao.Instance.SaveContentOff();
                    if (result != "OK")
                    {
                        
                        return Json(result);
                    }
                    else
                    {
                        return Json("Successfully !");
                    }
                }
                else
                {
                    return Json("Can Not Update Data value is not match!");
                }
            }
            catch (Exception e)
            {
                return Json("Error !!");
            }
        }
        public ActionResult UpdateContentOn(string id)
        {
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    string result = ContentListDao.Instance.SaveContentOn(Convert.ToInt32(id));
                    if (result != "OK")
                    {
                        return Json(result);
                    }
                    else
                    {
                        return Json("Successfully !");
                    }
                }
                else
                {
                    return Json("Not Update Data value is not match!");
                }
            }
            catch (Exception e)
            {
                return Json("Error !!");
            }
        }
    }
}

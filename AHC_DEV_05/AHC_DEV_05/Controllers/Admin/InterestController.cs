﻿using AHCBL.Component.Common;
using AHCBL.Dao.Admin;
using AHCBL.Dto.Admin;
using OfficeOpenXml;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_DEV_05.Controllers.Admin
{
    public class InterestController : Controller
    {
        Permission checkuser = new Permission();
        // GET: Interest
        public ActionResult Index(string keyword, int? page)
        {
            checkuser.chkrights("admin");

            var data = InterestDao.Instance.GetDataInterest();

            int rows = Util.NVLInt(Varible.Config.page_rows);

            TempData["data"] = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
            TempData["data1"] = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList();
            
            ViewBag.Rows = rows;
            ViewBag.Count = data.Count.ToString();
            Session["Count"] = data.Count.ToString();
            Session["data"] = TempData["data1"];
            return View(TempData["data"]);
        }

        public ActionResult ExportToExcel()
        {
            try
            {
                var data = Session["data"] as List<InterestDto>;
                if (data != null)
                {
                    ExcelPackage.LicenseContext = LicenseContext.Commercial;
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    ExcelPackage Ep = new ExcelPackage();
                    ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
                    Sheet.Cells["A1"].Value = "회원아이디 / รหัสสมาชิก";
                    Sheet.Cells["B1"].Value = "이름 / ชื่อ";
                    Sheet.Cells["C1"].Value = "이자금액 / ราคากำไร";
                    Sheet.Cells["D1"].Value = "판매한날짜 / วันที่ขาย";
                    int row = 2;
                    foreach (var item in data)
                    {
                        Sheet.Cells[string.Format("A{0}", row)].Value = item.username.ToString();
                        Sheet.Cells[string.Format("B{0}", row)].Value = item.fullname.ToString();
                        Sheet.Cells[string.Format("C{0}", row)].Value = item.interest.ToString();
                        Sheet.Cells[string.Format("D{0}", row)].Value = item.sale_date.ToString();
                        row++;
                    }
                    Sheet.Cells["A:AZ"].AutoFitColumns();
                    Response.Clear();
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment: filename=" + "Report.xlsx");
                    Response.BinaryWrite(Ep.GetAsByteArray());
                    Response.End();


                }
                ViewBag.Rows = Util.NVLInt(Varible.Config.page_rows);
                ViewBag.Count = Session["Count"];
                return View("Index");
            }
            catch (Exception e)
            {
                return RedirectToAction("Index");
            }
        }
    }
}
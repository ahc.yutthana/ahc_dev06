﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto.Admin;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class FAQController : Controller
    {
        Permission checkuser = new Permission();
        // GET: FAQ
        public ActionResult Index(int? page)
        {
            checkuser.chkrights("admin");
            var FAQ = FAQDao.Instance.GetFAQ();
            ViewBag.Count = FAQ.Count.ToString();
            return View(FAQ.ToList().ToPagedList(page ?? 1, Util.NVLInt(ConfigurationManager.AppSettings["rows"])));
        }
        public ActionResult Detail(int id)
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(FAQDto model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    HttpPostedFileBase img_head = Request.Files["img_head"];
                    HttpPostedFileBase img_tail = Request.Files["img_tail"];

                    var head = FileUploadDao.Instance.Upload(img_head);
                    var tail = FileUploadDao.Instance.Upload(img_tail);
                    model.img_head = head.name;
                    model.img_tail = tail.name;


                    string result = FAQDao.Instance.SaveFAQ(model, "add");
                    if (result != "OK")
                    {
                        ViewBag.Message = result;
                        ModelState.Clear();
                    }
                    else
                    {
                        //ViewBag.Status = TempData["Dropdown"];
                        ViewBag.Message = "Successfully !!";
                        //KRPBL.Component.Common.Form.SetAlertMessage(this,"ไม่พบ User ID ที่ระบุ กรุณาตรวจสอบ");
                        ModelState.Clear();
                    }
                }
                return View();
            }
            catch (Exception e)
            {
                ViewBag.Message = "Error : " + e.Message;
                return View();
            }
        }
        [HttpPost]
        public ActionResult UploadFiles(HttpPostedFileBase file)
        {
            FileUploadDao.Instance.Upload(file);
            return View();
        }
        
        public ActionResult Edit(int id)
        {
            return View(FAQDao.Instance.GetFAQ().Find(smodel => smodel.id == id));
        }
        [HttpPost]
        public ActionResult Edit(FAQDto model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    HttpPostedFileBase img_head = Request.Files["img_head"];
                    HttpPostedFileBase img_tail = Request.Files["img_tail"];

                    var head = FileUploadDao.Instance.Upload(img_head);
                    var tail = FileUploadDao.Instance.Upload(img_tail);
                    model.img_head = head.name;
                    model.img_tail = tail.name;
                    string result = FAQDao.Instance.SaveFAQ(model, "edit");
                    if (result != "OK")
                    {
                        ViewBag.Message = result;
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                    return View("Edit");
                }
                else
                {
                    return View();
                }
                
            }
            catch
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult Delete(FAQDto model)
        {
            try
            {
                string result = FAQDao.Instance.SaveFAQ(model, "del");
                if (result == "OK")
                {
                    ViewBag.Message = "Student Deleted Successfully";
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                ViewBag.Message = "Error : " + e.Message.ToString();
                return View();
            }
        }
    }
}
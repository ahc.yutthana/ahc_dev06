﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto.Admin;
using OfficeOpenXml;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class WithdrawalListController : Controller
    {
        Permission checkuser = new Permission();
        // GET: Withdrawal
        public ActionResult Index(string drp, string keyword, int? page)
        {
            checkuser.chkrights("admin");
            var data = WithdrawalListDao.Instance.GetDataList();
            var amount = GetDataDao.Instance.GetWithdrawalSumAmount("");
            var dropdown = DropdownDao.Instance.GetDrpPoint(0);
            int rows = Util.NVLInt(ConfigurationManager.AppSettings["rows"]);
            ViewBag.Status = DropdownDao.Instance.GetStatusTran();
            string count = string.Empty;
            string total = string.Empty;

            Session["data"] = data.ToList().ToPagedList(page ?? 1, rows);

            if (amount < 1000)
            {
                total = amount.ToString();
            }
            else
            {
                total = amount.ToString("0,0", CultureInfo.InvariantCulture);
            }
            if (data.Count < 1000)
            {
                count = data.Count.ToString();
            }
            else
            {
                count = data.Count.ToString("0,0", CultureInfo.InvariantCulture);
            }

            ViewBag.Amount = total;
            ViewBag.Count = count;
            ViewBag.Search = dropdown;
            ViewBag.Rows = rows;

            if (drp == "username")
            {
                Session["data"] = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, rows);
            }
            if (drp == "name")
            {
                Session["data"] = data.Where(x => x.name == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, rows);
            }
            Session["Status"] = DropdownDao.Instance.GetStatusTran();
            Session["Amount"] = total;
            Session["Count"] = count;
            Session["Search"] = dropdown;
            Session["Rows"] = rows;
            return View(Session["data"]);

            //return View(WithdrawalListDao.Instance.GetDataList());
        }
        public ActionResult Updatedata(int id, int status)
        {
            try
            {
                WithdrawalListDto model = new WithdrawalListDto();
                model.id = id;
                model.status = status;
                string result = WithdrawalListDao.Instance.SaveDataList(model,"edit");
                if (result != "OK")
                {
                    return Json(result);
                }
                else
                {
                    return Json("Successfully !");
                }

            }
            catch (Exception e)
            {
                return Json("Error !!");
            }
        }
        public ActionResult ExportToExcel()
        {
            try
            {
                var data = Session["data"] as List<WithdrawalListDto>;
                if (data != null)
                {
                    ExcelPackage.LicenseContext = LicenseContext.Commercial;
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    ExcelPackage Ep = new ExcelPackage();
                    ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
                    Sheet.Cells["A1"].Value = "회원아이디 / รหัสสมาชิก";
                    Sheet.Cells["B1"].Value = "이름 / ชื่อ";
                    Sheet.Cells["C1"].Value = "마블마블 내용 / เนื้อหามหัศจรรย์";
                    Sheet.Cells["D1"].Value = "예금주 / ชื่อธนาคาร";
                    Sheet.Cells["E1"].Value = "예금주 / ชื่อบัญชี";
                    Sheet.Cells["F1"].Value = "계좌번호 / เลขบัญชี";
                    Sheet.Cells["G1"].Value = "출금신청액 / จำนวนเงินที่ถอน";
                    Sheet.Cells["H1"].Value = "수수료 / ค่าธรรมเนียม";
                    Sheet.Cells["I1"].Value = "실지급액 / จ่ายจริง";
                    Sheet.Cells["J1"].Value = "상태 / สถานะ";
                    Sheet.Cells["K1"].Value = "일시 / สร้าง";
                    int row = 2;
                    foreach (var item in data)
                    {
                        Sheet.Cells[string.Format("A{0}", row)].Value = item.username;
                        Sheet.Cells[string.Format("B{0}", row)].Value = item.fullname;
                        Sheet.Cells[string.Format("C{0}", row)].Value = item.name;
                        Sheet.Cells[string.Format("D{0}", row)].Value = item.bank_name;
                        Sheet.Cells[string.Format("E{0}", row)].Value = item.acc_name;
                        Sheet.Cells[string.Format("F{0}", row)].Value = item.acc_no;
                        Sheet.Cells[string.Format("G{0}", row)].Value = item.total;
                        Sheet.Cells[string.Format("H{0}", row)].Value = item.charge;
                        Sheet.Cells[string.Format("I{0}", row)].Value = item.amount;
                        Sheet.Cells[string.Format("J{0}", row)].Value = (item.status == 1 ? "대기": "완료");
                        Sheet.Cells[string.Format("K{0}", row)].Value = item.create_date;
                        row++;
                    }
                    Sheet.Cells["A:AZ"].AutoFitColumns();
                    Response.Clear();
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment: filename=" + "Report.xlsx");
                    Response.BinaryWrite(Ep.GetAsByteArray());
                    Response.End();


                }
                ViewBag.Rows = Util.NVLInt(Varible.Config.page_rows);
                ViewBag.Count = Session["Count"];
                ViewBag.Amount = Session["Amount"];
                ViewBag.Count = Session["Count"];
                ViewBag.Search = Session["Search"];
                ViewBag.Rows = Session["Rows"];
                return View("Index");
            }
            catch (Exception e)
            {
                return RedirectToAction("Index");
            }
        }
    }
}
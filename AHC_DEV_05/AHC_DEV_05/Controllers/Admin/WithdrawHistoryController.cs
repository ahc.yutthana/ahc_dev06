﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto.Admin;
using OfficeOpenXml;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace AHC_DEV_05.Controllers.Admin
{
    public class WithdrawHistoryController : Controller
    {
        Permission checkuser = new Permission();
        // GET: WithdrawHistory
        public ActionResult Index(int? page)
        {
            checkuser.chkrights("admin");
            var data = PointListDao.Instance.GetWithdraw();
            return View(data.ToList().ToPagedList(page ?? 1, Util.NVLInt(ConfigurationManager.AppSettings["rows"])));
        }
    }
}
﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto.Admin;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class BoardListController : Controller
    {
        // GET: BoardList
        Permission checkuser = new Permission();        
        public ActionResult Index(int? page)
        {
            checkuser.chkrights("admin");
            var data = BoardListDao.Instance.GetDataList();
            string count = string.Empty;
            if (data.Count < 1000)
            {
                count = data.Count.ToString();
            }
            else
            {
                count = data.Count.ToString("0,0", CultureInfo.InvariantCulture);
            }
            ViewBag.Count = count;
            ViewBag.grp_id = DropdownDao.Instance.GetDataGroup(3);
            ViewBag.skin_id = DropdownDao.Instance.GetDataSkin("pc");
            ViewBag.skin_mobile_id = DropdownDao.Instance.GetDataSkin("mobile");
            ViewBag.device_id = DropdownDao.Instance.GetDataDrive();
            int rows = Convert.ToInt32(ConfigurationManager.AppSettings["rows"]);
            ViewBag.rows = rows;
            return View(data.ToList().ToPagedList(page ?? 1, Util.NVLInt(rows)));
        }

        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult Create()
        {
            ViewBag.grp_id = DropdownDao.Instance.GetDataGroup(3);
            ViewBag.device_id = DropdownDao.Instance.GetDataDrive();
            var No = DropdownDao.Instance.GetMemberNo();
            ViewBag.list_level = No;
            ViewBag.read_level = No;
            ViewBag.write_level = No;
            ViewBag.reply_level = No;
            ViewBag.comment_level = No;
            ViewBag.link_level = No;
            ViewBag.upload_level = No;
            ViewBag.download_level = No;
            ViewBag.html_level = No;
            ViewBag.use_secret = DropdownDao.Instance.GetUsecret();
            var config = Varible.Config;
            var use_cert = config.cert_use;
            if (use_cert != 0)
            {
                ViewBag.use_cert = DropdownDao.Instance.GetDataConfirm();
            }
            else
            {
                ViewBag.use_cert = "";
            }
            ViewBag.skin_id = DropdownDao.Instance.GetDataSkin("pc");
            ViewBag.skin_mobile_id = DropdownDao.Instance.GetDataSkin("mobile");
            ViewBag.skin = Util.NVLInt(ConfigurationManager.AppSettings["skin_id"]);
            ViewBag.skin_mobile = Util.NVLInt(ConfigurationManager.AppSettings["skin_mobile_id"]);
            ViewBag.head = Util.NVLString(ConfigurationManager.AppSettings["head"]);
            ViewBag.tail = Util.NVLString(ConfigurationManager.AppSettings["tail"]);
            ViewBag.page_rows = config.page_rows;
            ViewBag.gallery_id = DropdownDao.Instance.GetMemberNo();
            ViewBag.gallery_cols = Util.NVLInt(ConfigurationManager.AppSettings["gallery_cols"]);
            ViewBag.reply_order = DropdownDao.Instance.GetAnswer();
            ViewBag.sort_field = DropdownDao.Instance.GetSortField();
            return View();
        }

        [HttpPost]
        public ActionResult Create(BoardListDto model)
        {

            try
            {
                if (ModelState.IsValid)
                {

                    string result = BoardListDao.Instance.SaveDataList(model, "add");
                    if (result != "OK")
                    {
                        ViewBag.Message = result;
                        ModelState.Clear();
                    }
                    else
                    {
                        ViewBag.grp_id = DropdownDao.Instance.GetDataGroup(3);
                        ViewBag.device_id = DropdownDao.Instance.GetDataDrive();
                        var No = DropdownDao.Instance.GetMemberNo();
                        ViewBag.list_level = No;
                        ViewBag.read_level = No;
                        ViewBag.write_level = No;
                        ViewBag.reply_level = No;
                        ViewBag.comment_level = No;
                        ViewBag.link_level = No;
                        ViewBag.upload_level = No;
                        ViewBag.download_level = No;
                        ViewBag.html_level = No;
                        ViewBag.use_secret = DropdownDao.Instance.GetUsecret();
                        var config = Varible.Config;
                        var use_cert = config.cert_use;
                        if (use_cert != 0)
                        {
                            ViewBag.use_cert = DropdownDao.Instance.GetDataConfirm();
                        }
                        else
                        {
                            ViewBag.use_cert = "";
                        }
                        ViewBag.skin_id = DropdownDao.Instance.GetDataSkin("pc");
                        ViewBag.skin_mobile_id = DropdownDao.Instance.GetDataSkin("mobile");
                        ViewBag.skin = Util.NVLInt(ConfigurationManager.AppSettings["skin_id"]);
                        ViewBag.skin_mobile = Util.NVLInt(ConfigurationManager.AppSettings["skin_mobile_id"]);
                        ViewBag.head = Util.NVLString(ConfigurationManager.AppSettings["head"]);
                        ViewBag.tail = Util.NVLString(ConfigurationManager.AppSettings["tail"]);
                        ViewBag.page_rows = config.page_rows;
                        ViewBag.gallery_id = DropdownDao.Instance.GetMemberNo();
                        ViewBag.gallery_cols = Util.NVLInt(ConfigurationManager.AppSettings["gallery_cols"]);
                        ViewBag.reply_order = DropdownDao.Instance.GetAnswer();
                        ViewBag.sort_field = DropdownDao.Instance.GetSortField();
                        //ViewBag.Status = TempData["Dropdown"];
                        ViewBag.Message = "Successfully !!";
                        //KRPBL.Component.Common.Form.SetAlertMessage(this,"ไม่พบ User ID ที่ระบุ กรุณาตรวจสอบ");
                        ModelState.Clear();
                    }
                }
                return View();
            }
            catch (Exception e)
            {
                ViewBag.Message = "Error : " + e.Message;
                return View();
            }
        }

        // GET: ContentList/Edit/5
        public ActionResult Edit(int id)
        {
            var model = BoardListDao.Instance.GetDataList().Find(smodel => smodel.id == id);
            ViewBag.grp_id = DropdownDao.Instance.GetDataGroup(3);
            ViewBag.device_id = DropdownDao.Instance.GetDataDrive();
            var No = DropdownDao.Instance.GetMemberNo();
            ViewBag.list_level = No;
            ViewBag.read_level = No;
            ViewBag.write_level = No;
            ViewBag.reply_level = No;
            ViewBag.comment_level = No;
            ViewBag.link_level = No;
            ViewBag.upload_level = No;
            ViewBag.download_level = No;
            ViewBag.html_level = No;
            ViewBag.use_secret = DropdownDao.Instance.GetUsecret();
            var config = Varible.Config;
           
            ViewBag.use_cert = DropdownDao.Instance.GetDataConfirm();
           
            ViewBag.skin_id = DropdownDao.Instance.GetDataSkin("pc");
            ViewBag.skin_mobile_id = DropdownDao.Instance.GetDataSkin("mobile");
            ViewBag.skin = Util.NVLInt(ConfigurationManager.AppSettings["skin_id"]);
            ViewBag.skin_mobile = Util.NVLInt(ConfigurationManager.AppSettings["skin_mobile_id"]);
            ViewBag.head = Util.NVLString(ConfigurationManager.AppSettings["head"]);
            ViewBag.tail = Util.NVLString(ConfigurationManager.AppSettings["tail"]);
            ViewBag.page_rows = config.page_rows;
            ViewBag.gallery_id = DropdownDao.Instance.GetMemberNo();
            ViewBag.gallery_cols = Util.NVLInt(ConfigurationManager.AppSettings["gallery_cols"]);
            ViewBag.reply_order = DropdownDao.Instance.GetAnswer();
            ViewBag.sort_field = DropdownDao.Instance.GetSortField();
            return View(model);
        }

        // POST: ContentList/Edit/5
        [HttpPost]
        public ActionResult Edit(BoardListDto model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string result = BoardListDao.Instance.SaveDataList(model, "edit");
                    if (result != "OK")
                    {
                        ViewBag.Message = result;
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                    return View();

                } 
                else
                {
                    return View();
                }
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(BoardListDto model, string[] data)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    foreach (string id in data)
                    {
                        model.id = Util.NVLInt(id);
                        BoardListDao.Instance.SaveDataList(model, "del");
                    }
                    return Json("Deleted successfully !");
                }
                else
                {
                    return Json("Data not found !");
                }
            }
            catch (Exception e)
            {
                return Json("Error");
            }


            //try
            //{
            //    string result = BoardListDao.Instance.SaveDataList(model, "del");
            //    if (result == "OK")
            //    {
            //        ViewBag.Message = "Student Deleted Successfully";
            //    }
            //    return RedirectToAction("Index");
            //}
            //catch (Exception e)
            //{
            //    ViewBag.Message = "Error : " + e.Message.ToString();
            //    return View();
            //}
        }
    }
}

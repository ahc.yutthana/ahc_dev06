﻿using AHCBL.Component.Common;
using AHCBL.Dao.Admin;
using AHCBL.Dto;
using AHCBL.Dto.Admin;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class SendEmailController : Controller
    {
        Permission checkuser = new Permission();
        // GET: SendEmail
        public ActionResult Index(int? page)
        {
            checkuser.chkrights("admin");
            var data = SendEmailDao.Instance.GetDataList();
            ViewBag.Count = data.Count.ToString();
            return View(data.ToList().ToPagedList(page ?? 1, Util.NVLInt(ConfigurationManager.AppSettings["rows"])));
        }
        public ActionResult Create()
        {
            checkuser.chkrights("admin");
            return View();
        }
        [HttpPost]
        public ActionResult Create(SendEmailDto email)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    string result = SendEmailDao.Instance.SaveDataList(email, "add");
                    if (result != "OK")
                    {
                        ViewBag.Message = result;
                        ModelState.Clear();
                    }
                    else
                    {
                        ViewBag.Message = "Successfully !!";
                        ModelState.Clear();
                    }
                }
                return View();
            }
            catch (Exception e)
            {
                ViewBag.Message = "Error : " + e.Message;
                return View();
            }
        }        
        public ActionResult Detail(int id)
        {
            checkuser.chkrights("admin");
            return View(SendEmailDao.Instance.GetDataList().Find(smodel => smodel.id == id));
        }
        public ActionResult Edit(int id)
        {
            checkuser.chkrights("admin");
            return View(SendEmailDao.Instance.GetDataList().Find(smodel => smodel.id == id));
        }
        [HttpPost]
        public ActionResult Edit(SendEmailDto model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string result = SendEmailDao.Instance.SaveDataList(model, "edit");
                    if (result != "OK")
                    {
                        ViewBag.Message = result;
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                }
                
                return View();
            }
            catch
            {
                return View();
            }
        }
        public ActionResult SendForms()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SendForms(SendEmailDto model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string result = SendEmailDao.Instance.SaveDataList(model, "edit");
                    if (result != "OK")
                    {
                        ViewBag.Message = result;
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                }

                return View();
            }
            catch
            {
                return View();
            }
        }
        public ActionResult SendAdmin(int pUid)
        {
            try
            {
                var mail = SendEmailDao.Instance.GetDataList().Find(smodel => smodel.id == pUid);
                string result = SendEmailDao.Instance.Send(mail);
                if (result != "OK")
                {
                    return Json(new returnsave { err = "1", errmsg = "Error" });
                }
                else
                {
                    return Json(new returnsave { err = "0", errmsg = "최고관리자()님께 테스트 메일을 발송하였습니다. 확인하여 주십시오." });
                }
                
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "1", errmsg = "Error" });
            }
        }

        public ActionResult Delete(SendEmailDto model, string[] customerIDs)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    foreach (string id in customerIDs)
                    {
                        model.id = Util.NVLInt(id);
                        SendEmailDao.Instance.SaveDataList(model, "del");
                    }
                    return Json("Send email deleted successfully !");
                } 
                else
                {
                    return Json("Data not found !");
                }           
            }
            catch (Exception e)
            {
                return Json("Error");
            }
        }
    }
}
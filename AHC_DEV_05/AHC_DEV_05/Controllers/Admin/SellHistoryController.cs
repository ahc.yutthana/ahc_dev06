﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto.Admin;
using OfficeOpenXml;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class SellHistoryController : Controller
    {
        Permission checkuser = new Permission();
        // GET: RequestList

        // GET: SellHistory
        public ActionResult Index(string drp, string keyword, int? page)
        {
            Session["drp"] = DropdownDao.Instance.GetDrpReqBuy();
            ViewBag.Search = Session["drp"];
            checkuser.chkrights("admin");
            var data = RequestListDao.Instance.GetSellList();
            return View(data.ToList().ToPagedList(page ?? 1, Util.NVLInt(ConfigurationManager.AppSettings["rows"])));
            //return View(data);
        }
    }
}
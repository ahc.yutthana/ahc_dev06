﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto;
using AHCBL.Dto.Admin;
using OfficeOpenXml;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace AHC_MLK.Controllers.Admin
{
    public class RequestSellController : Controller
    {
        // GET: RequestSell
        public ActionResult Index(string drp, string keyword, int? page)
        {
            Session["drp"] = DropdownDao.Instance.GetDrpReqBuy();
            ViewBag.Search = Session["drp"];

            var data = RequestSellDao.Instance.GetDataRequestSellList();
            return View(data.ToList().ToPagedList(page ?? 1, Util.NVLInt(ConfigurationManager.AppSettings["rows"])));


        }
        public ActionResult getdata(string id,string member)
        {
            try
            {
                var Matching = RequestSellDao.Instance.GetDataMatching(id,member);
                ViewBag.Matching = Matching.ToList();
                ViewBag.MatchingCnt = Matching.Count();
                return Json(new returnsave { err = "0", errmsg = "OK" });
              
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "1", errmsg = "Error" });
            }

        }
        public ActionResult matchdata(int id,int id_sale,int amt_sale)
        {
            try
            {
                //Session.Remove("member_id");
                string result = RequestSellDao.Instance.ChkData(id, id_sale, amt_sale);
                if (result == "0")
                {
                    return Json(new returnsave { err = "1", errmsg = "Error." });
                }
                else if (result == "1")
                {

                    return Json(new returnsave { err = "0", errmsg = "@amt_buy > amt_sale" });
                }
                else if (result == "2")
                {
                    return Json(new returnsave { err = "0", errmsg = "@amt_buy < amt_sale" });
                }
                else
                {
                    return Json(new returnsave { err = "0", errmsg = "@amt_buy = amt_sale" });
                }
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "0", errmsg = "Error" });
            }
        }

    }
}